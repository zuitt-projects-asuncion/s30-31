const Task = require('../models/Task');

module.exports.createTaskController = (req, res) => {

	console.log(req.body);

	Task.findOne({name: req.body.name}).then(result => {

		if(result !== null && result.name === req.body.name){

			return res.send('Duplicate task found')

		} else {

			let newTask = new Task({

				name: req.body.name,
				status: req.body.status
			})

			newTask.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}
	})
	.catch(error => res.send(error));

};

module.exports.getAllTasksController = (req, res) => {

	//db.tasks.find({}) or get all under /tasks
	Task.find({})
		.then(result => res.send(result))
		.catch(error => res.send(error))
};