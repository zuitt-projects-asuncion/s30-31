const Task2 = require('../models/Task2');

module.exports.createTaskController = (req, res) => {

	console.log(req.body);

	Task2.findOne({name: req.body.name}).then(result => {

		if(result !== null && result.name === req.body.name){

			return res.send('Duplicate task found')

		} else {

			let newTask2 = new Task2({

				username: req.body.name,
				password: req.body.status
			})

			newTask2.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}
	})
	.catch(error => res.send(error));

};

module.exports.getAllTasksController = (req, res) => {

	//db.tasks.find({}) or get all under /tasks
	Task2.find({})
		.then(result => res.send(result))
		.catch(error => res.send(error))
};