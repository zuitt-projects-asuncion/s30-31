// importing express to use the Router() method
const express = require('express');

// allows us to access our HTTP method routes
const router = express.Router();

// importing taskControllers
const taskControllers = require('../controllers/taskControllers');

// create a task route
router.post('/', taskControllers.createTaskController2);

// retrieving all tasks route
router.get('/', taskControllers.getAllTasksController2);

module.exports = router;
